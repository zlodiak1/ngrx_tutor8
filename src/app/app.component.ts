import { Component, OnInit } from '@angular/core';
import { Cars } from './car.model'
import { Store, select } from '@ngrx/store';
import { AppState } from './redux/app.state';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

    public carState: Observable<AppState>

    constructor(private store: Store<Cars>) {

    }

    ngOnInit() {
       // this.store.pipe(select('carPage')).subscribe(
       //     ({cars}) => {
       //         this.cars = cars;
       //     }
       // ) 
       this.carState = this.store.pipe(select('carPage'));
    }

}
