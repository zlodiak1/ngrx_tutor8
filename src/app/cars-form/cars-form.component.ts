import { Component, OnInit, Output } from '@angular/core';
import { Car } from '../car.model'
import { Store } from '@ngrx/store';
import { AddCar } from '../redux/cars.action';
import { AppState } from '../redux/app.state';
import { CarsService } from '../cars.service';

@Component({
  selector: 'app-cars-form',
  templateUrl: './cars-form.component.html',
  styleUrls: ['./cars-form.component.css']
})
export class CarsFormComponent implements OnInit {

  carName: string = '';
  carModel: string = '';

  private id: number = 2;

  constructor(private store: Store<AppState>, private service: CarsService) { }

  ngOnInit() {
  }

  onAdd() {
      if(this.carModel === '' || this.carName === '') { return }
      this.id = this.id + 1;

      console.log(this.id)

      const car = new Car(
          this.carName,
          'date',
          this.carModel,
          false,
          this.id
      );

      this.store.dispatch(new AddCar(car));

      this.carModel = '';
      this.carName = '';
  }

  onLoad() {
    console.log('onLoad')
    this.service.LoadCars();
  }

}
