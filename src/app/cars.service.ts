import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { AppState } from './redux/app.state';
// import 'rxjs/add/operator/toPromise';
// import 'rxjs/add/operator/map';
import { map } from 'rxjs/operators';
import { Car } from './car.model';
import { LoadCars } from './redux/cars.action';

@Injectable()
export class CarsService {

    static BASE_URL: string = 'http://localhost:3000/';

    constructor(private http: HttpClient, private store: Store<AppState>) {}

    LoadCars(): void {
        const q = this.http.get(CarsService.BASE_URL + 'cars')
            .pipe(
                map((response: HttpResponse) => response)
            )

        q.subscribe(
            cars => {
                console.log(cars);
                this.store.dispatch(new LoadCars(cars));

            }
        );
    }

}